﻿using System;
using System.Collections.Generic;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PracticeAuto
{
    [TestClass]

    public class SeleniumEasy
    {
        private TestContext _testContextInstance;
        private IWebDriver _driver;
        private string _seleniumEasyUrl;
        private string _inputMessage;
        private string _yourMessage;
        private int _numA;
        private int _numB;
        private int _totalAB;
        private int _sumAB;
        private bool _isChecked;
        private string _correctString;
        private string _returnString;
        private string _firstSelectedOption;
        private string _secondSelectedOption;
        private List<IWebElement> _totalOption;
        private List<IWebElement> _totalSecondOption;
        private List<IWebElement> _totalSelectedOptions;

        private int SumTotalAB()
        {
            _sumAB = _numA + _numB;
            return _sumAB;
        }

        [TestMethod]
        [TestCategory("Chrome")]
        public void SeleniumEasy_Auto()
        {
            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));

            //Go to SeleniumEasy
            _driver.Navigate().GoToUrl(_seleniumEasyUrl);
            _driver.Manage().Window.Maximize();

            ////Test 1: Input Forms
            //wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='treemenu']/li/ul/li[1]/a"))).Click();

            ////Test 1.1: Single Form Demo
            //wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='treemenu']/li/ul/li[1]/ul/li[1]/a"))).Click();

            ////Test 1.1.1: Single Input Field
            //_inputMessage = "This is my message";
            //wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='user-message']"))).SendKeys(_inputMessage);
            //_driver.FindElement(By.XPath("//*[@id='get-input']/button")).Click();
            //_yourMessage = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='display']"))).Text;
            //if (_yourMessage != _inputMessage)
            //{
            //    _driver.Quit();
            //}

            ////Test 1.1.2: Two Input Fields
            //_numA = 1;
            //_numB = 2;
            //_driver.FindElement(By.XPath("//*[@id='sum1']")).SendKeys(_numA.ToString());
            //_driver.FindElement(By.XPath("//*[@id='sum2']")).SendKeys(_numB.ToString());
            //SumTotalAB();
            //_driver.FindElement(By.XPath("//*[@id='gettotal']/button")).Click();
            //_totalAB = int.Parse(wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='displayvalue']"))).Text);
            //if (_sumAB != _totalAB)
            //{
            //    _driver.Quit();
            //}

            ////Test 1.2: Checkbox Demo
            //wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='treemenu']/li/ul/li[1]/a"))).Click();
            //wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='treemenu']/li/ul/li[1]/ul/li[2]/a"))).Click();

            ////Test 1.2.1: Single Checkbox Demo
            //_isChecked = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='easycont']/div/div[2]/div[1]/div[2]/div[1]/label"))).Selected;
            //if (_isChecked == false)
            //{
            //    _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[1]/div[2]/div[1]/label")).Click();
            //}

            //_correctString = "Success - Check box is checked";
            //_returnString = _driver.FindElement(By.XPath("//*[@id='txtAge']")).Text;
            //if (_returnString != _correctString)
            //{
            //    _driver.Quit();
            //}

            ////Test 1.2.2: Multiple Checkbox Demo
            //_totalOption = new List<IWebElement>(_driver.FindElements(By.XPath("//*[@id='easycont']/div/div[2]/div[2]/div[2]//input[@type='checkbox']")));
            //for (var i = 1; i <= _totalOption.Capacity; i++)
            //{
            //    _isChecked = _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[2]/div[2]/div[" + i + "]/label/input")).Selected;
            //    if (_isChecked)
            //    {
            //        _driver.Quit();
            //    }
            //}

            //_correctString = "Check All";
            //_returnString = _driver.FindElement(By.XPath("//*[@id='check1']")).GetAttribute("value");
            //if (_returnString == _correctString)
            //{
            //    _driver.FindElement(By.XPath("//*[@id='check1']")).Click();
            //}
            //else
            //{
            //    _driver.Quit();
            //}

            //_correctString = "Uncheck All";
            //_returnString = _driver.FindElement(By.XPath("//*[@id='check1']")).GetAttribute("value");
            //if (_returnString != _correctString)
            //{
            //    _driver.Quit();
            //}

            //_totalOption = new List<IWebElement>(_driver.FindElements(By.XPath("//*[@id='easycont']/div/div[2]/div[2]/div[2]//input[@type='checkbox']")));
            //for (var i = 1; i <= _totalOption.Capacity; i++)
            //{
            //    _isChecked = _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[2]/div[2]/div[" + i + "]/label/input")).Selected;
            //    if (!_isChecked)
            //    {
            //        _driver.Quit();
            //    }
            //}

            ////Test 1.3: Radio Buttons Demo
            //_driver.FindElement(By.XPath("//*[@id='treemenu']/li/ul/li[1]/a")).Click();
            //_driver.FindElement(By.XPath("//*[@id='treemenu']/li/ul/li[1]/ul/li[3]/a")).Click();

            ////Test 1.3.1: Radio Button Demo
            //wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='buttoncheck']"))).Click();
            //_correctString = "Radio button is Not checked";
            //_returnString = _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[1]/div[2]/p[3]")).Text;
            //if (_returnString != _correctString)
            //{
            //    _driver.Quit();
            //}

            //_totalOption = new List<IWebElement>(_driver.FindElements(By.XPath("//*[@id='easycont']/div/div[2]/div[1]/div[2]//input[@type='radio']")));
            //for (var i = 1; i <= _totalOption.Capacity; i++)
            //{
            //    _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[1]/div[2]/label[" + i + "]/input")).Click();
            //    _driver.FindElement(By.XPath("//*[@id='buttoncheck']")).Click();
            //    _firstSelectedOption = _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[1]/div[2]/label[" + i + "]")).Text;
            //    _correctString = "Radio button '" + _firstSelectedOption + "' is checked";
            //    _returnString = _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[1]/div[2]/p[3]")).Text;
            //    if (_returnString != _correctString)
            //    {
            //        _driver.Quit();
            //    }
            //}

            ////Test 1.3.2: Group Radio Buttons Demo
            //_totalOption = new List<IWebElement>(_driver.FindElements(By.XPath("//*[@id='easycont']/div/div[2]/div[2]/div[2]/div[1]//input[@type='radio']")));
            //_totalSecondOption = new List<IWebElement>(_driver.FindElements(By.XPath("//*[@id='easycont']/div/div[2]/div[2]/div[2]/div[2]//input[@type='radio']")));
            //for (var i = 1; i <= _totalOption.Capacity; i++)
            //{
            //    _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[2]/div[2]/div[1]/label[" + i + "]/input")).Click();
            //    _firstSelectedOption = _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[2]/div[2]/div[1]/label[" + i + "]")).Text;
            //    for (var j = 1; j <= _totalSecondOption.Capacity; j++)
            //    {
            //        _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[2]/div[2]/div[2]/label[" + j + "]/input")).Click();
            //        _secondSelectedOption = (_driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[2]/div[2]/div[2]/label[" + j + "]")).Text).Replace("to", "-");
            //        _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[2]/div[2]/button")).Click();
            //        _correctString = "Sex : " + _firstSelectedOption + "\r\nAge group: " + _secondSelectedOption;
            //        _returnString = _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[2]/div[2]/p[2]")).Text;
            //        if (_returnString != _correctString)
            //        {
            //            _driver.Quit();
            //        }
            //    }
            //}

            //Test 1.4: Select Dropdown List
            _driver.FindElement(By.XPath("//*[@id='treemenu']/li/ul/li[1]/a")).Click();
            _driver.FindElement(By.XPath("//*[@id='treemenu']/li/ul/li[1]/ul/li[4]/a")).Click();

            ////Test 1.4.1: Select List Demo
            //_totalOption = new List<IWebElement>(_driver.FindElements(By.XPath("//*[@id='select-demo']/option")));
            //for (var i = 1; i <= _totalOption.Capacity; i++)
            //{
            //    if (Convert.ToBoolean(_driver.FindElement(By.XPath("//*[@id='select-demo']/option[" + i + "]")).GetAttribute("disabled")))
            //    {
            //        continue;
            //    }

            //    _driver.FindElement(By.XPath("//*[@id='select-demo']/option[" + i + "]")).Click();
            //    _correctString = "Day selected :- " + _driver.FindElement(By.XPath("//*[@id='select-demo']/option[" + i + "]")).Text;
            //    _returnString = _driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[1]/div[2]/p[2]")).Text;
            //    if (_returnString != _correctString)
            //    {
            //        _driver.Quit();
            //    }
            //}

            //Test 1.4.2: Multi Select List Demo
            _totalOption = new List<IWebElement>(_driver.FindElements(By.XPath("//*[@id='multi-select']//input[@type='option']")));
            for (var i = 1; i <= _totalOption.Capacity; i++)
            {

            }
        }

        public TestContext TestContext
        {
            get => _testContextInstance;
            set => _testContextInstance = value;
        }

        [TestInitialize()]
        public void SetupTest()
        {
            _seleniumEasyUrl = "https://www.seleniumeasy.com/test/";

            //Open Chrome in Incognito mode
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--incognito");

            //Enable logging
            //var verboseLog = ChromeDriverService.CreateDefaultService();
            //verboseLog.LogPath = @"D:\ChromeDriver.log";
            //verboseLog.EnableVerboseLogging = true;

            //Driver
            //_driver = new ChromeDriver(verboseLog, options);
            _driver = new ChromeDriver(options);

            //Waiting variables 
            _driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            _driver.Quit();
        }
    }
}